FROM node:14.5.0

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci

COPY ./dist ./dist

EXPOSE 3000

CMD ["npm", "run", "start"]
