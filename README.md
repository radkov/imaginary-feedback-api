# Imaginary Feedback API

A TypeScript and express API that collects feedbacks after users stay in an imaginary hotel.

## How to run

1. Clone the project and navigate to the main directory
    ```bash
    $ git clone https://gitlab.com/radkov/imaginary-feedback-api.git && cd imaginary-feedback-api
    ```
2. `$ npm ci` - installs the dependencies
3. `$ npm run start:watch` - runs the server at the port specified in `.env` file
   and watches for file changes
4. `$ npm run lint` - runs the eslint and markdown linters
5. `$ npm run test:watch` - runs tests in watch mode
6. `$ npm run test` - runs tests only once
7. `$ npm run build` - builds the production version of the API
   which will be generated in the `dist` folder
8. `$ npm run start` - runs the server from the dist folder at the port specified in `.env`
9. Build a docker image
    ```bash
    # ifa === Imaginary Feedback API
    $ docker build -t ifa .
    ```
10. Run a container and redirect container's exposed port to host's port 9000
    ```bash
    $ docker run --env-file=.env --name imaginary-api -p 9000:3000 -d ifa
    ```

## Technical stack

1. **node** - runtime environment
2. **express** - for building the API
3. **typescript** - the project is build with TypeScript
4. **uuid** - for creating a unique id while the feedback is saving
5. **dotenv** - for injecting environment variables at runtime
6. **winston** - for logging to console and to a file
7. **nodemon** - for watching the file changes while local development
8. **jest** - testing framework
9. **eslint** - for unifying the TypeScript code
10. **markdownlint** - for unifying the markdown documents
11. **prettier** - for auto formatting while saving a file

## Endpoints

There are only a few endpoints in the API which are guarded by Basic Authentication and
Role Based Access Control.

### POST /submit

This endpoint is used for users to submit their feedback about their stay in the imaginary hotel.
The endpoint is guarded with Basic Authentication and can be used only by users with **basic**
or **admin** role.

The hardcoded basic user credentials are:

```JSON
{
    "username": "user",
    "password": "password",
}
```

#### Request Headers

> `Authentication: Basic dXNlcjpwYXNzd29yZA==`

#### Request Body

```JSON
{
    "authorName": "Jack",
    "dateCreated": 1595677846629,
    "body":  "5 stars from me. Super nice experience and I will come back again next year!!!"
}
```

#### Response Body

```JSON
{
    "authorName": "Jack",
    "dateCreated": 1595677846629,
    "body":  "5 stars from me. Super nice experience and I will come back again next year!!!",
    "id": "75a904c6-e0e4-4d8c-98b4-899a96e610a1"
}
```

---

### GET /feedback?byName=\<name\>&fromDate=\<from_timestamp\>&toDate=\<to_timestamp\>

The endpoint is used by users with **admin** role for filtering feedbacks by **byName**,
**fromDate**and **toDate**.
The three query params are all optional and can be used individually or in a combination.

The hardcoded admin user credentials are:

```JSON
{
    "username": "admin",
    "password": "secretPassword",
}
```

#### Request Headers

> `Authentication: Basic YWRtaW46c2VjcmV0UGFzc3dvcmQ=`

#### Response Body

```JSON
[
    {
        "authorName": "Jack",
        "dateCreated": 1595677846629,
        "body": "5 stars from me. Super nice experience and I will come back again next year!!!",
        "id": "75a904c6-e0e4-4d8c-98b4-899a96e610a1"
    },
    {
        "authorName": "Jack",
        "dateCreated": 1595677846630,
        "body": "This year was more than wonderful. :)))",
        "id": "f2e9e246-4e07-4bab-9b80-e90ca11d0425"
    }
]
```
