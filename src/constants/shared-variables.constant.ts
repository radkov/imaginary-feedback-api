import { IDefaultEnvironmentVariables } from '../interfaces/shared-variables.interface';

export const requiredEnvironmentVariables = [
    'NODE_ENV',
    'PORT',
    'STORAGE_TYPE',
    'STORAGE_NAME',
    'NORMAL_USERNAME',
    'NORMAL_PASSWORD',
    'ADMIN_USERNAME',
    'ADMIN_PASSWORD',
];

export const defaultSharedVariables: IDefaultEnvironmentVariables = {
    environment: 'development',
    port: '3000',
    version: '1',
    loggerType: 'winston',
    loggerLevel: 'silly',
    loggerName: 'api-logs.log',
    storageType: 'file',
    storageName: 'imaginary-feedback',
    formatSpaces: '2',
};
