const unauthorizedError = 401;
const forbiddenError = 403;
const internalServerError = 500;

export enum HttpStatusCodes {
    Unauthorized = unauthorizedError,
    Forbidden = forbiddenError,
    InternalServerError = internalServerError,
}
