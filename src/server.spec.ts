import { configureImaginaryFeedbackServer } from './server';
import { getLogger } from './services/logger/logger.service';
import { mockedSharedVariables } from './services/shared-variables/shared-varaibles.service.mock';

describe('configureImaginaryFeedbackServer()', () => {
    it('should return Express object when called with correct parameters', async () => {
        const logger = getLogger(mockedSharedVariables);

        const result = await configureImaginaryFeedbackServer(mockedSharedVariables, logger);

        expect(result).toHaveProperty('listen');
        expect(result.listen).toBeInstanceOf(Function);
    });
});
