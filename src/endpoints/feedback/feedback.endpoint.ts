import { Request, Response, NextFunction } from 'express';

import { IFeedbackService } from '../../interfaces/feedback.interface';
import { IFindFilters } from './../../interfaces/storage.interface';
import { ILogger } from './../../interfaces/logger.interface';

export const getFeedbackEndpoint = (feedbackService: IFeedbackService, logger: ILogger) => async (
    request: Request,
    response: Response,
    next: NextFunction,
): Promise<void> => {
    try {
        const filters: IFindFilters = {
            authorName: request.query.byName as string,
            fromDate: request.query.fromDate ? parseInt(request.query.fromDate as string, 10) : undefined,
            toDate: request.query.toDate ? parseInt(request.query.toDate as string, 10) : undefined,
        };
        logger.log('Filtering feedbacks', (filters as unknown) as Record<string, unknown>);

        const foundFeedbacks = await feedbackService.filterFeedbacks(filters);
        response.send(foundFeedbacks);

        logger.log('Filtering done');
        next();
    } catch (error) {
        logger.error('Error during filtering feedbacks');
        next(error);
    }
};
