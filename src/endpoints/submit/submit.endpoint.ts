import { Request, Response, NextFunction } from 'express';

import { getFeedbackPropertyErrorMessage } from './../../utils/error.util';
import { IFeedbackService, IFeedback } from './../../interfaces/feedback.interface';
import { checkRequiredProperties } from '../../utils/error.util';
import { ILogger } from './../../interfaces/logger.interface';

export const getSubmitEndpoint = (
    feedbackService: IFeedbackService,
    requiredProperties: string[],
    logger: ILogger,
) => async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    try {
        logger.log('Checking if posted feedback is correct');
        checkRequiredProperties(request.body, requiredProperties, getFeedbackPropertyErrorMessage);

        const feedback: IFeedback = {
            authorName: request.body.authorName,
            dateCreated: request.body.dateCreated,
            body: request.body.body,
        };
        logger.log('Saving feedback to storage', (feedback as unknown) as Record<string, unknown>);

        const storedFeedback = await feedbackService.submitFeedback(feedback);
        response.send(storedFeedback);

        logger.log('Feedback saved');
        next();
    } catch (error) {
        logger.error('Error during new feedback processing');
        next(error);
    }
};
