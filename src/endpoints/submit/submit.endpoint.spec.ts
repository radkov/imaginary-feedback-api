import mocks from 'node-mocks-http';

import { getSubmitEndpoint } from './submit.endpoint';
import { IFeedbackService } from './../../interfaces/feedback.interface';
import { expressMiddleware } from './../../interfaces/express.interface';
import { ILogger } from './../../interfaces/logger.interface';

describe('Submit Endpoint', () => {
    describe('getSubmitEndpoint()', () => {
        let middleware: expressMiddleware;
        let mockLogger: ILogger;
        let mockFeedbackService: IFeedbackService;
        const mockFeedback = {
            dateCreated: 1,
            authorName: 'Tester',
            body: 'Long time a go in a galaxy far far away',
        };

        beforeEach(() => {
            mockLogger = {
                log: jest.fn(),
                warn: jest.fn(),
                error: jest.fn(),
            };
            mockFeedbackService = {
                submitFeedback: jest.fn(() => Promise.resolve(mockFeedback)),
                filterFeedbacks: jest.fn(() => Promise.resolve([mockFeedback])),
            };
            middleware = getSubmitEndpoint(mockFeedbackService, [], mockLogger);
        });

        it('should return a function with 3 arguments', () => {
            const expectedFunctionArgumentsLength = 3;
            expect(middleware).toBeInstanceOf(Function);
            expect(middleware.length).toBe(expectedFunctionArgumentsLength);
        });

        it('should return a function which handle an error and pass it to the next', async () => {
            const request = mocks.createRequest();
            const response = mocks.createResponse();
            const nextStub = jest.fn();
            const rejectionError = 'Cannot save feedback';
            mockFeedbackService.submitFeedback = jest.fn(() => Promise.reject(rejectionError));

            await middleware(request, response, nextStub);

            expect(mockLogger.error).toBeCalled();
            expect(nextStub).toBeCalledWith(rejectionError);
        });

        it('should return a function which sends found feedbacks', async () => {
            const request = mocks.createRequest();
            const response = mocks.createResponse();
            const nextStub = jest.fn();
            const sendSpy = jest.spyOn(response, 'send');

            await middleware(request, response, nextStub);

            expect(mockLogger.log).toBeCalled();
            expect(sendSpy).toBeCalledWith(mockFeedback);
        });
    });
});
