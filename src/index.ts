import { env } from 'process';

import { configureImaginaryFeedbackServer } from './server';
import { getSharedVariables } from './services/shared-variables/shared-variables.service';
import { getLogger } from './services/logger/logger.service';
import { defaultSharedVariables } from './constants/shared-variables.constant';
import { requiredEnvironmentVariables } from './constants/shared-variables.constant';

const sharedVariables = getSharedVariables(env, defaultSharedVariables, requiredEnvironmentVariables);
const logger = getLogger(sharedVariables);

const startServer = async (): Promise<void> => {
    try {
        const server = await configureImaginaryFeedbackServer(sharedVariables, logger);
        server.listen(sharedVariables.port, () =>
            logger.log(`Imaginary feedback server listening at port ${sharedVariables.port}`),
        );
    } catch (error) {
        logger.error('Cannot start the server', error);
    }
};

startServer();
