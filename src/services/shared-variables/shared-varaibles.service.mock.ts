import { IDefaultEnvironmentVariables } from './../../interfaces/shared-variables.interface';
import { ISharedVariables } from '../../interfaces/shared-variables.interface';
import { LoggerType } from './../../constants/logger.constant';
import { StorageType } from '../../constants/storage.constant';

export const mockedSharedVariables: ISharedVariables = {
    environment: 'test',
    port: 5000,
    version: 1,
    loggerLevel: 'silly',
    loggerType: LoggerType.Winston,
    loggerName: 'mock-log',
    storageType: StorageType.File,
    storageName: 'mock-feedback',
    formatSpaces: 2,
    normalUsername: 'user',
    normalPassword: 'pass',
    adminUsername: 'admin',
    adminPassword: 'pass1',
};

export const mockedDefaultEnvironmentVariables: IDefaultEnvironmentVariables = {
    environment: 'test',
    port: '5000',
    version: '1',
    loggerLevel: 'silly',
    loggerType: LoggerType.Winston,
    loggerName: 'mock-log',
    storageType: StorageType.File,
    storageName: 'mock-feedback',
    formatSpaces: '2',
    normalUsername: 'user',
    normalPassword: 'pass',
    adminUsername: 'admin',
    adminPassword: 'pass1',
};
