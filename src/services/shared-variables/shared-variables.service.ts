import { getEnumValue, getStringValue, getNumberValue } from './../../utils/parser.util';
import { checkRequiredProperties } from './../../utils/error.util';
import {
    IEnvironmentVariables,
    ISharedVariables,
    IDefaultEnvironmentVariables,
} from '../../interfaces/shared-variables.interface';
import { getRequiredVariableErrorMessage } from '../../utils/error.util';
import { StorageType } from '../../constants/storage.constant';
import { LoggerType } from '../../constants/logger.constant';

export const getSharedVariables = (
    environmentVariables: IEnvironmentVariables,
    defaultSharedVariables: IDefaultEnvironmentVariables,
    requiredVariables: string[],
): ISharedVariables => {
    checkRequiredProperties(environmentVariables, requiredVariables, getRequiredVariableErrorMessage);

    return {
        environment: getStringValue(environmentVariables.NODE_ENV, defaultSharedVariables.environment),
        port: getNumberValue(environmentVariables.PORT, defaultSharedVariables.port),
        version: getNumberValue(environmentVariables.VERSION, defaultSharedVariables.version),
        loggerType: getEnumValue<LoggerType>(environmentVariables.LOGGER_TYPE, defaultSharedVariables.loggerType),
        loggerLevel: getStringValue(environmentVariables.LOGGER_LEVEL, defaultSharedVariables.loggerLevel),
        loggerName: getStringValue(environmentVariables.LOGGER_NAME, defaultSharedVariables.loggerName),
        storageName: getStringValue(environmentVariables.STORAGE_NAME, defaultSharedVariables.storageName),
        storageType: getEnumValue<StorageType>(environmentVariables.STORAGE_TYPE, defaultSharedVariables.storageType),
        formatSpaces: getNumberValue(environmentVariables.FORMAT_SPACES, defaultSharedVariables.formatSpaces),
        normalUsername: getStringValue(environmentVariables.NORMAL_USERNAME, defaultSharedVariables.normalUsername),
        normalPassword: getStringValue(environmentVariables.NORMAL_PASSWORD, defaultSharedVariables.normalPassword),
        adminUsername: getStringValue(environmentVariables.ADMIN_USERNAME, defaultSharedVariables.adminUsername),
        adminPassword: getStringValue(environmentVariables.ADMIN_PASSWORD, defaultSharedVariables.adminPassword),
    };
};
