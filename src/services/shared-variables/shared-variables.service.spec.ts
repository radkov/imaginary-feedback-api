import { getSharedVariables } from './shared-variables.service';
import { mockedSharedVariables, mockedDefaultEnvironmentVariables } from './shared-varaibles.service.mock';

describe('Shared Variables Service', () => {
    describe('getSharedVariables()', () => {
        it('should return configurations when correct data is passed', async () => {
            const sharedVariables = getSharedVariables({}, mockedDefaultEnvironmentVariables, []);
            expect(sharedVariables).toStrictEqual(mockedSharedVariables);
        });

        it('should throw an error when one of the required env variables is missing', async () => {
            expect(() => getSharedVariables({}, mockedDefaultEnvironmentVariables, ['MOCKED'])).toThrow();
        });
    });
});
