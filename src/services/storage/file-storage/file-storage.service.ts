import { appendFile, access, constants, writeFile, createReadStream } from 'fs';
import readline from 'readline';
import { promisify } from 'util';
import { v4 as uuidv4 } from 'uuid';
import mkdirp from 'mkdirp';
import path from 'path';

import { IFeedback } from './../../../interfaces/feedback.interface';
import { ISharedVariables } from './../../../interfaces/shared-variables.interface';
import { IStorage, IFindFilters } from './../../../interfaces/storage.interface';
import { storageFolder } from '../../../constants/storage.constant';

export const getFileStorage = async (sharedVariables: ISharedVariables): Promise<IStorage> => {
    const appendFileAsync = promisify(appendFile);
    const accessFileAsync = promisify(access);
    const writeFileAsync = promisify(writeFile);

    const storageFilePath = path.join(storageFolder, sharedVariables.storageName);

    const ensureFileExists = async (): Promise<void> => {
        try {
            await accessFileAsync(storageFilePath, constants.F_OK);
        } catch (error) {
            await mkdirp(storageFolder);
            await writeFileAsync(storageFilePath, '', { flag: 'wx' });
        }
    };

    await ensureFileExists();

    const isSameAuthor = (feedback: IFeedback, author?: string): boolean => {
        return !author || feedback.authorName === author;
    };

    const isInDateRange = (feedback: IFeedback, startDate?: number, endDate?: number): boolean => {
        const isBiggerThanStartDate = !startDate || startDate <= feedback.dateCreated;
        const isSmallerThanEndDate = !endDate || feedback.dateCreated <= endDate;

        return isBiggerThanStartDate && isSmallerThanEndDate;
    };

    const isFeedbackMachingFilter = (feedback: IFeedback, filters: IFindFilters): boolean => {
        const isAuthorMatch = isSameAuthor(feedback, filters.authorName);
        const isDateRangeMatch = isInDateRange(feedback, filters.fromDate, filters.toDate);

        return isAuthorMatch && isDateRangeMatch;
    };

    const findAll = async (filters: IFindFilters = {}): Promise<IFeedback[]> => {
        await ensureFileExists();

        const fileStream = createReadStream(storageFilePath);
        const fileLines = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity,
        });
        const foundFeedbacks = [];

        for await (const line of fileLines) {
            const feedback = JSON.parse(line);
            const isMatch = isFeedbackMachingFilter(feedback, filters);
            if (isMatch) {
                foundFeedbacks.push(feedback);
            }
        }

        return foundFeedbacks;
    };

    const save = async (feedback: IFeedback): Promise<IFeedback> => {
        await ensureFileExists();

        const feedbackToSave = {
            ...feedback,
            id: uuidv4(),
        };
        const feedbackLine = `${JSON.stringify(feedbackToSave)}\n`;
        await appendFileAsync(storageFilePath, feedbackLine);

        return feedbackToSave;
    };

    return {
        findAll,
        save,
    };
};
