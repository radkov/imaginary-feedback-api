import { promisify } from 'util';
import { IStorage } from './../../../interfaces/storage.interface';
import { getFileStorage } from './file-storage.service';
import { mockedSharedVariables } from '../../shared-variables/shared-varaibles.service.mock';
import { IFeedback } from '../../../interfaces/feedback.interface';
import { unlink } from 'fs';
import path from 'path';

describe('File Storage Service', () => {
    describe('getFileStorage()', () => {
        const unlinkAsync = promisify(unlink);
        let fileStorage: IStorage;

        beforeEach(async () => {
            fileStorage = await getFileStorage(mockedSharedVariables);
        });

        afterEach(async () => {
            await unlinkAsync(path.resolve('storage', mockedSharedVariables.storageName));
        });

        it('should return storage when correct sharedVariables are passed', async () => {
            expect(fileStorage).toBeDefined();
        });

        it('should has save function which returns the same object with new id', async () => {
            expect(fileStorage.save).toBeInstanceOf(Function);
            const resultAfterSave = await fileStorage.save({} as IFeedback);
            expect(resultAfterSave).toHaveProperty('id');
            expect(typeof resultAfterSave.id).toBe('string');
        });

        it('should has findAll function which returns filtered feedbacks by authorName', async () => {
            const authorName = 'Joe';
            const resultAfterSave = await fileStorage.save({
                dateCreated: 1,
                authorName,
                body: 'Test',
            });

            expect(fileStorage.findAll).toBeInstanceOf(Function);
            const resultAfterFindAllByName = await fileStorage.findAll({
                authorName,
            });
            expect(resultAfterFindAllByName).toStrictEqual([resultAfterSave]);
        });

        it('should has findAll function which returns filtered feedbacks by date range', async () => {
            const dateCreated = 1;
            const resultAfterSave = await fileStorage.save({
                dateCreated,
                authorName: 'Joe',
                body: 'Test',
            });

            await fileStorage.save({
                dateCreated: 3,
                authorName: 'Joe 2',
                body: 'Test 2',
            });

            const resultAfterFindAllByName = await fileStorage.findAll({
                fromDate: dateCreated,
                toDate: 2,
            });
            expect(resultAfterFindAllByName).toStrictEqual([resultAfterSave]);
        });

        it('should has findAll function which returns all feedbacks when no criteria is passed', async () => {
            await fileStorage.save({
                dateCreated: 1,
                authorName: 'Joe',
                body: 'Test',
            });
            await fileStorage.save({
                dateCreated: 2,
                authorName: 'Joe 2',
                body: 'Test 2',
            });
            await fileStorage.save({
                dateCreated: 3,
                authorName: 'Joe 3',
                body: 'Test 3',
            });

            const expectedFeedbacksLength = 3;
            const resultAfterFindAllByName = await fileStorage.findAll();
            expect(resultAfterFindAllByName).toHaveLength(expectedFeedbacksLength);
        });
    });
});
