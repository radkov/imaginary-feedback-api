import { ISharedVariables } from './../../interfaces/shared-variables.interface';
import { IStorage } from '../../interfaces/storage.interface';
import { getFileStorage } from './file-storage/file-storage.service';

export const getStorage = async (sharedVariables: ISharedVariables): Promise<IStorage> => {
    const storageInitializer = {
        file: getFileStorage,
    };
    const storageType = sharedVariables.storageType;

    if (!storageInitializer.hasOwnProperty(storageType)) {
        throw new Error(`${storageType} storage is not configured`);
    }

    const storage = await storageInitializer[storageType](sharedVariables);
    return storage;
};
