import { StorageType } from './../../constants/storage.constant';
import { getStorage } from './storage.service';
import { mockedSharedVariables } from './../shared-variables/shared-varaibles.service.mock';

describe('Storage Service', () => {
    describe('getStorage()', () => {
        it('should return storage when a specific storage is configured', async () => {
            const storage = await getStorage(mockedSharedVariables);
            expect(storage).toBeDefined();
            expect(storage.save).toBeInstanceOf(Function);
            expect(storage.findAll).toBeInstanceOf(Function);
        });

        it('should throw an error when a storage type is not configured', () => {
            const sharedVariables = {
                ...mockedSharedVariables,
                storageType: ('mocked' as unknown) as StorageType,
            };

            expect(async () => await getStorage(sharedVariables)).rejects.toThrow('mocked storage is not configured');
        });
    });
});
