import { IStorage } from './../../interfaces/storage.interface';
import { getFeedbackService } from './feedback.service';
import { IFeedback } from '../../interfaces/feedback.interface';

describe('Feedback Service', () => {
    describe('getFeedbackService()', () => {
        it('should return feedback service using mock storage under the hood', async () => {
            const mockFeedback: IFeedback = {
                dateCreated: 1,
                authorName: 'Jack',
                body: 'Test feedback',
            };
            const mockStorage: IStorage = {
                save: () => Promise.resolve(mockFeedback),
                findAll: () => Promise.resolve([mockFeedback]),
            };

            const feedbackService = getFeedbackService(mockStorage);
            expect(feedbackService).toBeDefined();

            expect(feedbackService.submitFeedback).toBeInstanceOf(Function);
            const resultAfterSubmit = await feedbackService.submitFeedback(mockFeedback);
            expect(resultAfterSubmit).toStrictEqual(mockFeedback);

            expect(feedbackService.filterFeedbacks).toBeInstanceOf(Function);
            const resultAfterFilter = await feedbackService.filterFeedbacks({});
            expect(resultAfterFilter).toStrictEqual([mockFeedback]);
        });
    });
});
