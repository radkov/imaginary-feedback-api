import { IStorage } from './../../interfaces/storage.interface';
import { IFeedbackService, IFeedback } from '../../interfaces/feedback.interface';
import { IFindFilters } from '../../interfaces/storage.interface';

export const getFeedbackService = (storage: IStorage): IFeedbackService => {
    const submitFeedback = async (feedback: IFeedback): Promise<IFeedback> => {
        return await storage.save(feedback);
    };

    const filterFeedbacks = async (filters: IFindFilters): Promise<IFeedback[]> => {
        return await storage.findAll(filters);
    };

    return {
        submitFeedback,
        filterFeedbacks,
    };
};
