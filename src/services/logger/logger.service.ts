import { ILogger, ILoggerInitializer } from './../../interfaces/logger.interface';
import { getWinstonLogger } from './winston-logger/winston-logger.service';
import { ISharedVariables } from '../../interfaces/shared-variables.interface';

export const getLogger = (sharedVariables: ISharedVariables): ILogger => {
    const loggerInitializer: ILoggerInitializer = {
        winston: getWinstonLogger,
    };

    const loggerType = sharedVariables.loggerType;

    if (!loggerInitializer.hasOwnProperty(loggerType)) {
        throw new Error(`${loggerType} logger is not configured`);
    }

    const logger = loggerInitializer[loggerType](sharedVariables);
    return logger;
};
