import { getLogger } from './logger.service';
import { mockedSharedVariables } from './../shared-variables/shared-varaibles.service.mock';
import { LoggerType } from '../../constants/logger.constant';

describe('Logger Service', () => {
    describe('getLogger()', () => {
        it('should return logger when a specific loger is configured', () => {
            const logger = getLogger(mockedSharedVariables);
            expect(logger).toBeDefined();
            expect(logger.log).toBeInstanceOf(Function);
            expect(logger.warn).toBeInstanceOf(Function);
            expect(logger.error).toBeInstanceOf(Function);
        });

        it('should throw an error when a loger type is not configured', () => {
            const sharedVariables = {
                ...mockedSharedVariables,
                loggerType: ('mocked' as unknown) as LoggerType,
            };

            expect(() => getLogger(sharedVariables)).toThrow('mocked logger is not configured');
        });
    });
});
