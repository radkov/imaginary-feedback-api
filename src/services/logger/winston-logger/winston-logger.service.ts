import winston, { format, transports } from 'winston';
import path from 'path';

import { ILogger } from './../../../interfaces/logger.interface';
import { ISharedVariables } from './../../../interfaces/shared-variables.interface';
import { logsFolder } from '../../../constants/logger.constant';

export const getWinstonLogger = (sharedVariables: ISharedVariables): ILogger => {
    const loggerFile = path.resolve(logsFolder, sharedVariables.loggerName);
    const logger = winston.createLogger({
        level: sharedVariables.loggerLevel,
        transports: [
            new transports.Console({
                format: format.combine(
                    winston.format.timestamp(),
                    format.metadata(),
                    winston.format.simple(),
                    winston.format.printf((msg) => {
                        const {
                            level,
                            message,
                            metadata: { timestamp, ...metadata },
                        } = msg;
                        const stringifiedMetadata = Object.keys(metadata).length
                            ? JSON.stringify(metadata, null, sharedVariables.formatSpaces)
                            : '';

                        return winston.format
                            .colorize()
                            .colorize(level, `${timestamp} | [${level}]: ${message} ${stringifiedMetadata}`);
                    }),
                ),
            }),
            new transports.File({
                filename: loggerFile,
                format: format.combine(winston.format.timestamp(), format.metadata(), format.json()),
            }),
        ],
    });

    const log = (message: string, metadata?: Record<string, unknown>) => {
        logger.info(message, metadata);
    };

    const warn = (message: string, metadata?: Record<string, unknown>) => {
        logger.warn(message, metadata);
    };

    const error = (message: string, metadata?: Record<string, unknown>) => {
        logger.error(message, metadata);
    };

    return {
        log,
        warn,
        error,
    };
};
