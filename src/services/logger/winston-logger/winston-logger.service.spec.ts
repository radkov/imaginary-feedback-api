import { getWinstonLogger } from './winston-logger.service';
import { mockedSharedVariables } from '../../shared-variables/shared-varaibles.service.mock';

describe('Winston Logger Service', () => {
    describe('getWinstonLogger()', () => {
        it('should return logger using Winston under the hood', () => {
            const logger = getWinstonLogger(mockedSharedVariables);
            expect(logger).toBeDefined();

            expect(logger.log).toBeInstanceOf(Function);
            expect(logger.log('Testing', { name: 'Winston' })).toBeUndefined();

            expect(logger.warn).toBeInstanceOf(Function);
            expect(logger.warn('Warn Testing')).toBeUndefined();

            expect(logger.error).toBeInstanceOf(Function);
            expect(logger.error('Error!!!')).toBeUndefined();
        });
    });
});
