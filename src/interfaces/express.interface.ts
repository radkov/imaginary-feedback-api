import { Request, Response, NextFunction } from 'express';

import { IUser } from './user.interface';

export interface IUserDecoratedRequest extends Request {
    user: IUser;
}

export type expressMiddleware = (request: Request, response: Response, next: NextFunction) => void;
export type expressErrorMiddleware = (error: Error, request: Request, response: Response, next: NextFunction) => void;
