import { ISharedVariables } from './shared-variables.interface';

export type loggerFunction = (message: string, metadata?: Record<string, unknown>) => void;
export interface ILogger {
    log: loggerFunction;
    error: loggerFunction;
    warn: loggerFunction;
}
export type ILoggerInitializer = Record<string, (sharedVariables: ISharedVariables) => ILogger>;
