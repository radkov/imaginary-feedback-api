import { StorageType } from '../constants/storage.constant';
import { LoggerType } from '../constants/logger.constant';

export interface IEnvironmentVariables {
    [ket: string]: string | undefined;
}

export interface IDefaultEnvironmentVariables {
    [ket: string]: string;
}

export interface ISharedVariables {
    environment: string;
    port: number;
    version: number;
    loggerType: LoggerType;
    loggerLevel: string;
    loggerName: string;
    storageType: StorageType;
    storageName: string;
    formatSpaces: number;
    normalUsername: string;
    normalPassword: string;
    adminUsername: string;
    adminPassword: string;
}
