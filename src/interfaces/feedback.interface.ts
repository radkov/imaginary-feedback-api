import { IFindFilters } from './storage.interface';

export interface IFeedback {
    id?: string;
    authorName: string;
    body: string;
    dateCreated: number;
}

export interface IFeedbackService {
    submitFeedback: (feedback: IFeedback) => Promise<IFeedback>;
    filterFeedbacks: (filters: IFindFilters) => Promise<IFeedback[]>;
}
