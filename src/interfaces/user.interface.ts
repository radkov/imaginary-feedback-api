import { UserRole } from './../constants/authentication.constant';

export interface IUser extends IBasicUser {
    role: UserRole;
}

export interface IBasicUser {
    username: string;
    password: string;
}
