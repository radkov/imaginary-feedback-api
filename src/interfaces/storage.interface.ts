import { IFeedback } from './feedback.interface';

export interface IStorage {
    findAll: (filters?: IFindFilters) => Promise<IFeedback[]>;
    save: (feedback: IFeedback) => Promise<IFeedback>;
}

export interface IFindFilters {
    authorName?: string;
    fromDate?: number;
    toDate?: number;
}
