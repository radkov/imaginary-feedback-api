import { expressErrorMiddleware } from './../../interfaces/express.interface';
import mocks from 'node-mocks-http';

import { HttpStatusCodes } from './../../constants/http.constant';
import { ILogger } from './../../interfaces/logger.interface';
import { getErrorHandlerMiddleware } from './error-handler.middleware';

describe('Error Handler Middleware', () => {
    describe('getErrorHandlerMiddleware()', () => {
        let middleware: expressErrorMiddleware;
        let mockLogger: ILogger;

        beforeEach(() => {
            mockLogger = {
                log: jest.fn(),
                warn: jest.fn(),
                error: jest.fn(),
            };
            middleware = getErrorHandlerMiddleware(mockLogger);
        });

        it('should return a function with 4 arguments', () => {
            const expectedFunctionArgumentsLength = 4;
            expect(middleware).toBeInstanceOf(Function);
            expect(middleware.length).toBe(expectedFunctionArgumentsLength);
        });

        it('should return a function which sets status code 500 to response', () => {
            const request = mocks.createRequest();
            const response = mocks.createResponse();
            const statusResponseSpy = jest.spyOn(response, 'status');

            middleware({ message: 'Error1' } as Error, request, response, jest.fn());

            expect(statusResponseSpy).toBeCalled();
            expect(response.statusCode).toBe(HttpStatusCodes.InternalServerError);
        });
    });
});
