import { Request, Response, NextFunction } from 'express';

import { ILogger } from './../../interfaces/logger.interface';
import { HttpStatusCodes } from '../../constants/http.constant';

export const getErrorHandlerMiddleware = (logger: ILogger) => (
    error: Error,
    request: Request,
    response: Response,
    next: NextFunction,
): void => {
    const { message, stack, name } = error;
    logger.error(message, {
        message,
        stack,
        name,
    });
    response.status(HttpStatusCodes.InternalServerError);
    response.send({ error: message });
    next();
};
