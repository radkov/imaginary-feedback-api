import mocks from 'node-mocks-http';

import { getAuthenticationMiddleware } from './authentication.middleware';
import { expressMiddleware } from './../../interfaces/express.interface';
import { HttpStatusCodes } from './../../constants/http.constant';
import { ILogger } from './../../interfaces/logger.interface';
import { UserRole } from '../../constants/authentication.constant';
import { mockedSharedVariables } from '../../services/shared-variables/shared-varaibles.service.mock';

describe('Authentication Middleware', () => {
    describe('getAuthenticationMiddleware()', () => {
        let middleware: expressMiddleware;
        let mockLogger: ILogger;

        beforeEach(() => {
            mockLogger = {
                log: jest.fn(),
                warn: jest.fn(),
                error: jest.fn(),
            };
            middleware = getAuthenticationMiddleware(mockedSharedVariables, mockLogger);
        });

        it('should return a function with 3 arguments', () => {
            const expectedFunctionArgumentsLength = 3;
            expect(middleware).toBeInstanceOf(Function);
            expect(middleware.length).toBe(expectedFunctionArgumentsLength);
        });

        it('should return a function which sets status code 401 to response when user credentials are invalid', () => {
            const request = mocks.createRequest();
            request.headers.authorization = 'Basic Test';
            const response = mocks.createResponse();
            const statusResponseSpy = jest.spyOn(response, 'status');

            middleware(request, response, jest.fn());

            expect(statusResponseSpy).toBeCalled();
            expect(response.statusCode).toBe(HttpStatusCodes.Unauthorized);
        });

        it('should return a function which attaches user to request when user has correct credentials', () => {
            const request = mocks.createRequest();
            request.headers.authorization = 'Basic YWRtaW46cGFzczE=';
            const response = mocks.createResponse();
            const nextStub = jest.fn();

            middleware(request, response, nextStub);

            expect(mockLogger.log).toBeCalled();
            expect(request.user).toStrictEqual({
                username: 'admin',
                password: 'pass1',
                role: UserRole.Admin,
            });
        });
    });
});
