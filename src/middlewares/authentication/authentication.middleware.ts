import { expressMiddleware } from './../../interfaces/express.interface';
import { Request, Response, NextFunction } from 'express';

import { ISharedVariables } from './../../interfaces/shared-variables.interface';
import { HttpStatusCodes } from '../../constants/http.constant';
import { UserRole } from '../../constants/authentication.constant';
import { IUserDecoratedRequest } from '../../interfaces/express.interface';
import { IUser } from './../../interfaces/user.interface';
import { ILogger } from './../../interfaces/logger.interface';
import { extractUserFromBasicAuth, isValidCredentials } from '../../utils/authentication.util';

export const getAuthenticationMiddleware = (sharedVariables: ISharedVariables, logger: ILogger): expressMiddleware => {
    const systemUsers: IUser[] = [
        {
            username: sharedVariables.normalUsername,
            password: sharedVariables.normalPassword,
            role: UserRole.Basic,
        },
        {
            username: sharedVariables.adminUsername,
            password: sharedVariables.adminPassword,
            role: UserRole.Admin,
        },
    ];

    return (request: Request, response: Response, next: NextFunction): void => {
        try {
            const authUser = extractUserFromBasicAuth(request.headers.authorization);
            const matchedUser = systemUsers.find((systemUser) => isValidCredentials(authUser, systemUser));

            if (matchedUser) {
                logger.log(`Access granted for ${matchedUser.role} user: "${matchedUser.username}"`);
                (request as IUserDecoratedRequest).user = {
                    ...matchedUser,
                };
                next();

                return;
            }

            logger.error(`Access denied for user ${authUser.username}`);
            response.set('WWW-Authenticate', 'Basic realm="Feedback System"');
            response.status(HttpStatusCodes.Unauthorized).send({
                error: 'Authentication required for accessing Imaginary Feedback API.',
            });
        } catch (error) {
            logger.error('Error during authentication');
            next(error);
        }
    };
};
