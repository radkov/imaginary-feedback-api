import { ILogger } from '../../interfaces/logger.interface';
import { Request, Response, NextFunction } from 'express';

import { UserRole } from '../../constants/authentication.constant';
import { HttpStatusCodes } from '../../constants/http.constant';
import { expressMiddleware, IUserDecoratedRequest } from '../../interfaces/express.interface';

export const getRoleBasedAccessControlMiddleware = (logger: ILogger) => (
    middleware: expressMiddleware,
    roles: UserRole[],
) => (request: Request, response: Response, next: NextFunction): void => {
    const { username, role: userRole } = (request as IUserDecoratedRequest).user;
    const userHasPermissions = roles.includes(userRole);

    if (!userHasPermissions) {
        logger.error(`Insufficient permissions for user: "${username}", role: "${userRole}". Access denied!`);
        response.status(HttpStatusCodes.Forbidden);
        response.send({
            error: "You don't have permissions to access the resource",
        });
        return;
    }
    logger.log(`User: "${username}", role: "${userRole}" accessing ${request.originalUrl}`);
    middleware(request, response, next);
};
