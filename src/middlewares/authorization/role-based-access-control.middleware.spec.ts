import mocks from 'node-mocks-http';

import { getRoleBasedAccessControlMiddleware } from './role-based-access-control.middleware';
import { expressMiddleware } from './../../interfaces/express.interface';
import { HttpStatusCodes } from './../../constants/http.constant';
import { ILogger } from './../../interfaces/logger.interface';
import { UserRole } from '../../constants/authentication.constant';

describe('Role Based Access Control Middleware', () => {
    describe('getRoleBasedAccessControlMiddleware()', () => {
        let middleware: expressMiddleware;
        let mockLogger: ILogger;
        let innerMiddleware: expressMiddleware;
        beforeEach(() => {
            mockLogger = {
                log: jest.fn(),
                warn: jest.fn(),
                error: jest.fn(),
            };
            innerMiddleware = jest.fn();
            middleware = getRoleBasedAccessControlMiddleware(mockLogger)(innerMiddleware, [UserRole.Admin]);
        });

        it('should return a function with 3 arguments', () => {
            const expectedFunctionArgumentsLength = 3;
            expect(middleware).toBeInstanceOf(Function);
            expect(middleware.length).toBe(expectedFunctionArgumentsLength);
        });

        it("should return a function which sets status code 403 to response when user don't have correct role", () => {
            const request = mocks.createRequest();
            request.user = {
                username: 'man',
                role: UserRole.Basic,
            };
            const response = mocks.createResponse();
            const statusResponseSpy = jest.spyOn(response, 'status');

            middleware(request, response, jest.fn());

            expect(statusResponseSpy).toBeCalled();
            expect(response.statusCode).toBe(HttpStatusCodes.Forbidden);
        });

        it('should return a function which executes innerMiddleware when user has correct role', () => {
            const request = mocks.createRequest();
            request.user = {
                username: 'man',
                role: UserRole.Admin,
            };
            const response = mocks.createResponse();
            const nextStub = jest.fn();

            middleware(request, response, nextStub);

            expect(mockLogger.log).toBeCalled();
            expect(innerMiddleware).toBeCalledWith(request, response, nextStub);
        });
    });
});
