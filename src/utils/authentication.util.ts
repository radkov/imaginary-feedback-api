import { IUser, IBasicUser } from './../interfaces/user.interface';
export const extractUserFromBasicAuth = (authorizationHeader = ''): IBasicUser => {
    const authParts = authorizationHeader.split(' ');
    const base64auth = authParts.pop() || '';
    const [username, password] = Buffer.from(base64auth, 'base64').toString().split(':');

    return { username, password };
};

export const isValidCredentials = (authUser: IBasicUser, systemUser: IUser): boolean =>
    !!authUser.username &&
    !!authUser.password &&
    authUser.username === systemUser.username &&
    authUser.password === systemUser.password;
