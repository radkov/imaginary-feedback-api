import { LoggerType } from './../constants/logger.constant';
import { getStringValue, getNumberValue, getEnumValue } from './parser.util';
describe('Parser Util', () => {
    describe('getStringValue()', () => {
        it('should get the first argument when the first argument is a string', () => {
            expect(getStringValue('first', 'default')).toBe('first');
            expect(getStringValue('second', 'default')).not.toBe('default');
        });

        it('should get the second argument when the first argument is nil', () => {
            expect(getStringValue((null as unknown) as undefined, 'default')).toBe('default');
            expect(getStringValue(undefined, 'default')).toBe('default');
        });

        it('should get the second argument when the first argument is not string', () => {
            expect(getStringValue((true as unknown) as undefined, 'default')).toBe('default');
        });
    });

    describe('getNumberValue()', () => {
        it('should parse the first argument when the first argument is a string', () => {
            const expectedResult = 25;
            expect(getNumberValue('25', '15')).toBe(expectedResult);
            expect(getNumberValue('1', '25')).not.toBe(expectedResult);
        });

        it('should parse the second argument when the first argument is nil', () => {
            const expectedResult = 1;
            expect(getNumberValue((null as unknown) as undefined, '1')).toBe(expectedResult);
            expect(getNumberValue(undefined, '1')).toBe(expectedResult);
        });

        it('should parse the second argument when the first argument is not string', () => {
            const expectedResult = 27;
            expect(getNumberValue((true as unknown) as undefined, '27')).toBe(expectedResult);
        });
    });

    describe('getEnumValue<T>()', () => {
        it('should cast the first argument to enum when the first argument is not nil', () => {
            expect(getEnumValue<LoggerType>('winston', 'default')).toBe(LoggerType.Winston);
            expect(getEnumValue<LoggerType>('winston', 'default')).not.toBe('default');
        });

        it('should cast the second argument to enum when the first argument is nil', () => {
            expect(getEnumValue<LoggerType>((null as unknown) as undefined, 'winston')).toBe(LoggerType.Winston);
            expect(getEnumValue<LoggerType>(undefined, 'winston')).toBe(LoggerType.Winston);
        });
    });
});
