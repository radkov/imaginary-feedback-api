import { IBasicUser, IUser } from './../interfaces/user.interface';
import { extractUserFromBasicAuth, isValidCredentials } from './authentication.util';
import { UserRole } from '../constants/authentication.constant';

describe('Authentication Util', () => {
    describe('extractUserFromBasicAuth()', () => {
        it('should extract username and password from authorization header', () => {
            expect(extractUserFromBasicAuth('Basic dXNlcjpwYXNzd29yZA==')).toStrictEqual({
                username: 'user',
                password: 'password',
            });

            expect(extractUserFromBasicAuth('dXNlcjo=')).toStrictEqual({
                username: 'user',
                password: '',
            });
        });

        it('should extract empty username and undefined password when header not passed', () => {
            expect(extractUserFromBasicAuth()).toStrictEqual({
                username: '',
                password: undefined,
            });
        });
    });

    describe('isValidCredentials()', () => {
        it('should return true when the two users are identical', () => {
            const authUser: IBasicUser = {
                username: 'buffon',
                password: 'best-gk-ever',
            };
            const adminUser: IUser = {
                username: 'buffon',
                password: 'best-gk-ever',
                role: UserRole.Admin,
            };
            expect(isValidCredentials(authUser, adminUser)).toBe(true);
        });
    });
});
