import {
    getRequiredVariableErrorMessage,
    getFeedbackPropertyErrorMessage,
    checkRequiredProperties,
} from './error.util';

describe('Error Util', () => {
    describe('getRequiredVariableErrorMessage()', () => {
        it('should return interpolated required variable error message with passed argument', () => {
            expect(getRequiredVariableErrorMessage('name')).toContain('name');
            expect(getRequiredVariableErrorMessage('')).toContain('');
            expect(getRequiredVariableErrorMessage('age')).toContain('age');
        });
    });

    describe('getFeedbackPropertyErrorMessage()', () => {
        it('should return interpolated required feedback property error message with passed argument', () => {
            expect(getFeedbackPropertyErrorMessage('name')).toContain('name');
            expect(getFeedbackPropertyErrorMessage('')).toContain('');
            expect(getFeedbackPropertyErrorMessage('age')).toContain('age');
        });
    });

    describe('checkRequiredProperties()', () => {
        it('should throw an error if a required property is missing', () => {
            const objectToCheck = {};
            const requiredProperties = ['required'];
            const getErrorMessage = getRequiredVariableErrorMessage;
            expect(() => checkRequiredProperties(objectToCheck, requiredProperties, getErrorMessage)).toThrow(
                'required is a required variable',
            );
        });

        it('should not throw if all required properties are presented', () => {
            const objectToCheck = { required: true, name: 'Jack' };
            const requiredProperties = ['required', 'name'];
            const getErrorMessage = getRequiredVariableErrorMessage;
            expect(() => checkRequiredProperties(objectToCheck, requiredProperties, getErrorMessage)).not.toThrow();
        });
    });
});
