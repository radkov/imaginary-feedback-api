import { errorMessageCreator } from '../interfaces/error.interface';

export const getRequiredVariableErrorMessage: errorMessageCreator = (fieldName: string): string =>
    `${fieldName} is a required variable`;
export const getFeedbackPropertyErrorMessage: errorMessageCreator = (propertyName: string): string =>
    `${propertyName} must be pass to the request's body`;

export const checkRequiredProperties = (
    objectToCheck: Record<string, unknown>,
    requiredProperties: string[],
    getErrorMessage: errorMessageCreator,
): void => {
    const missingVariable = requiredProperties.find(
        (requiredVariable) => !objectToCheck.hasOwnProperty(requiredVariable),
    );

    if (missingVariable) {
        const errorMessage = getErrorMessage(missingVariable);
        throw new Error(errorMessage);
    }
};
