export const getStringValue = (value: string | undefined, defaultValue: string): string =>
    value && typeof value === 'string' ? value : defaultValue;

export const getNumberValue = (value: string | undefined, defaultValue: string): number => {
    const valueToParse = value && typeof value === 'string' ? value : defaultValue;
    return parseInt(valueToParse, 10);
};

export const getEnumValue = <T>(value: string | undefined, defaultValue: string): T =>
    ((value || defaultValue) as unknown) as T;
