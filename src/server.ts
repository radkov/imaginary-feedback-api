import express, { Express } from 'express';

import { ILogger } from './interfaces/logger.interface';
import { getFeedbackService } from './services/feedback/feedback.service';
import { getStorage } from './services/storage/storage.service';
import { ISharedVariables } from './interfaces/shared-variables.interface';
import { getSubmitEndpoint } from './endpoints/submit/submit.endpoint';
import { getFeedbackEndpoint } from './endpoints/feedback/feedback.endpoint';
import { getAuthenticationMiddleware } from './middlewares/authentication/authentication.middleware';
import { getRoleBasedAccessControlMiddleware } from './middlewares/authorization/role-based-access-control.middleware';
import { UserRole } from './constants/authentication.constant';
import { getErrorHandlerMiddleware } from './middlewares/error-handler/error-handler.middleware';
import { requiredFeedbackProperties } from './constants/feedback.constant';

export const configureImaginaryFeedbackServer = async (
    sharedVariables: ISharedVariables,
    logger: ILogger,
): Promise<Express> => {
    logger.log('Configuring server started');

    const storage = await getStorage(sharedVariables);
    const feedbackService = getFeedbackService(storage);

    const authenticationMiddleware = getAuthenticationMiddleware(sharedVariables, logger);
    const accessControl = getRoleBasedAccessControlMiddleware(logger);
    const submitMiddleware = getSubmitEndpoint(feedbackService, requiredFeedbackProperties, logger);
    const feedbackMiddleware = getFeedbackEndpoint(feedbackService, logger);
    const errorHandlerMiddleWare = getErrorHandlerMiddleware(logger);

    const imaginaryFeedbackServer = express();
    imaginaryFeedbackServer
        .use(express.json({ limit: '10mb' }))
        .use(authenticationMiddleware)
        .post('/submit', accessControl(submitMiddleware, [UserRole.Basic, UserRole.Admin]))
        .get('/feedback', accessControl(feedbackMiddleware, [UserRole.Admin]))
        .get('/', (_, response) => {
            logger.log('Base route called');
            response.send(`Imaginary Feedback API Version ${sharedVariables.version}`);
        })
        .use(errorHandlerMiddleWare);

    logger.log('Server configured sucessfully');

    return imaginaryFeedbackServer;
};
