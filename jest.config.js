module.exports = {
    roots: ['<rootDir>/src'],
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
    moduleFileExtensions: ['ts', 'js', 'json', 'node'],
    collectCoverage: true,
    collectCoverageFrom: [
        'src/**/*.ts',
        '!node_modules/**/*',
        '!src/**/*.spec.ts',
        '!src/**/*.mock.ts',
        '!src/**/*.constant.ts',
        '!src/**/*.interface.ts',
        '!src/index.ts',
    ],
    globals: {
        'ts-jest': {
            tsConfig: './tsconfig.spec.json',
            __TS_CONFIG__: './tsconfig.spec.json',
            stringifyContentPathRegex: true,
        },
    },
    moduleDirectories: ['node_modules'],
    coverageDirectory: './reports',
    coverageReporters: ['json', 'lcov', 'html', 'text', 'text-summary'],
    testRegex: 'src/.*spec\\.(ts?)$',
};
